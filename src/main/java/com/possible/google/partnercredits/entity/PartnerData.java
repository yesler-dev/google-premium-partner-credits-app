package com.possible.google.partnercredits.entity;

import java.util.Date;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

@Entity
public class PartnerData {
	@Parent Key<PartnerDataRecord> record;
	@Id public String pcnCode;
	@Index public String partnerName;
	public String partnerDescription;
	public String website;
	public boolean cloudPlatformPartner;
	public Date dt; 
	
	
	/**
	 * Simple constructor just sets the date
	 **/
	public PartnerData() {
		
	}
	  /**
	   * Takes all important fields	
	   **/
	  public PartnerData(String pcnCode) {
	    this();
	    record = Key.create(PartnerDataRecord.class, "partnerdata");
	    this.pcnCode = pcnCode;
	    this.dt = new Date();
	   
	  }	  
	  
	  public PartnerData(String pcnCode, String partnerName, String partnerDescription, String website, boolean cloudPlatformPartner, Date dt) {
	    record = Key.create(PartnerDataRecord.class, "partnerdata");
	    this.pcnCode = pcnCode;
	    this.partnerName = partnerName;
	    this.partnerDescription = partnerDescription;
	    this.website = website;
	    this.cloudPlatformPartner = cloudPlatformPartner;
	    this.dt = dt;	    
	  }
	
}