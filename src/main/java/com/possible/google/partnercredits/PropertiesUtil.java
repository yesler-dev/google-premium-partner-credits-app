package com.possible.google.partnercredits;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtil
{
    private Properties properties;

    public PropertiesUtil(){
        load();
    }
    
    protected void load() 
    {
		InputStream is =  this.getClass().getClassLoader().getResourceAsStream("application.properties");
        properties = new Properties();
        try {
			properties.load(is);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    public Properties getProperties()
    {
        return properties;
    }

    public String getProperty(String propertyName)
    {
        return (String) 
            getProperties().get(propertyName);
    }
}