package com.possible.google.partnercredits.email;

import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.possible.google.partnercredits.PropertiesUtil;


public class EmailHelper {
	private static Logger log = Logger.getLogger(EmailHelper.class.getName());
	
	public static void sendNotificationMail(int size){
		PropertiesUtil constants = new PropertiesUtil();

        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);
 
        String msgBody ="<html><body><p>Please be prepared to top up the coupon codes.</p></body></html>";
        String fromName = constants.getProperty("email.from-name");
    	String fromAddress = constants.getProperty("email.from-address");
    	String to = constants.getProperty("email.to");
    	String subject = replaceTag(constants.getProperty("email.subject"), "[NUMBER]", size+"");
 
        try {
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(fromAddress, fromName));
            msg.addRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            msg.setSubject(subject);
            msg.setContent(msgBody, "text/html;charset=\"UTF-8\"");
            Transport.send(msg);
            log.info("Notification sent to " + constants.getProperty("email.to") + " from " + fromName + " (" + fromAddress + " ) with subject " + subject);
        } catch (Exception e) {
            log.severe("Error sending notification email. Check your settings.");
            throw new RuntimeException(e);
        }
	}
	
	/**
	 * This method used to replace place holder from the content with specific
	 * string.
	 * 
	 * @param content
	 * @param tag
	 * @param value
	 * @return replaced Content as String
	 */
	public static String replaceTag(String content, String tag, String value) {
		int i = 0;
		while ((i = content.indexOf(tag)) != -1)
			content = content.substring(0, i) + value
					+ content.substring(i + tag.length(), content.length());

		tag = null;
		value = null;

		return content;
	}
}