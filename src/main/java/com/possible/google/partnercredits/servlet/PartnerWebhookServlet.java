package com.possible.google.partnercredits.servlet;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.possible.google.partnercredits.PropertiesUtil;
import com.possible.google.partnercredits.entity.PartnerData;
import com.possible.google.partnercredits.entity.PartnerDataRecord;

@SuppressWarnings("serial")
public class PartnerWebhookServlet extends HttpServlet {
	
	private static Logger log = Logger.getLogger(PartnerWebhookServlet.class.getName());
	
	// Process the http POST of the form
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		String token = req.getParameter("token");
		String pcn = req.getParameter("pcn");
		
		PropertiesUtil props = new PropertiesUtil();
		if (token == null || (token != null && !token.equals(props.getProperty("security.token")))) {
			//Display the error message
			log.severe("Invalid token. " + token);
			resp.setContentType("text/plain");
			resp.getWriter().println("{\"status\":\"Invalid Token\"}");
		} else {
			if(pcn != null && !pcn.isEmpty()){
				defaultAction(req, resp);	
			}else{
				//Display the error message
				log.severe("No PCN specified. ");
				resp.setContentType("text/plain");
				resp.getWriter().println("{\"status\":\"No PCN specified.\"}");
			}
			
		}
		
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doPost(req, resp);
	}

	private void defaultAction(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		String pcn = req.getParameter("pcn");
		PropertiesUtil props = new PropertiesUtil();
		// Retrieve partner information from the datastore based on the pcn code.
		final Key<PartnerDataRecord> partnerRecord = Key.create(PartnerDataRecord.class, "partnerdata");
		System.out.println("partnerRecord.getId()  " + partnerRecord.getId());
		List<PartnerData> partnerList = ObjectifyService.ofy().cache(false).load()
		//PartnerData partner = ObjectifyService.ofy().cache(false).load()
				.type(PartnerData.class)
				//.ancestor(partnerRecord)
				//.filter("pcnCode", pcn)
				//.limit(COUPON_THRESHOLD) // Only show 1 of them.
				.filterKey(Key.create(partnerRecord, PartnerData.class, pcn))
				.list();
		
		PartnerData partnerData = null;
		String pcnCode = null;
		if (partnerList != null && !partnerList.isEmpty()) {
			partnerData = partnerList.get(0);

			if (partnerData.pcnCode.equals(pcn)) {
				pcnCode = partnerData.pcnCode;
				log.info("####### PARTNER DATA TO USE: " + pcnCode);
				
				// Print out the JSon success response.
				resp.setContentType("text/plain");
				resp.getWriter().println(
						"{\"status\":\"success\",\"data\":{"
						+ "\"partner-name\":\"" + partnerData.partnerName + "\","
						+ "\"partner-description\":\"" + partnerData.partnerDescription + "\","
						+ "\"partner-website\":\"" + partnerData.website + "\""
						+ "}}");
			} else{
				// Display the status response.
				log.warning("Error: Record with PCN Code not found.");
				resp.setContentType("text/plain");
				resp.getWriter().println("{\"status\":\"Error: Record with PCN Code not found.\"}");
			}
		}else{
			// Display the status response.
			log.warning("Error: Record with PCN Code not found.");
			resp.setContentType("text/plain");
			resp.getWriter().println("{\"status\":\"Error: Record with PCN Code not found.\"}");
		}
	}
}