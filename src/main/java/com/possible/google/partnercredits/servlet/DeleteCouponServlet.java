package com.possible.google.partnercredits.servlet;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.possible.google.partnercredits.PropertiesUtil;
import com.possible.google.partnercredits.entity.Coupon;
import com.possible.google.partnercredits.entity.CouponData;

@SuppressWarnings("serial")
public class DeleteCouponServlet extends HttpServlet {

	private static Logger log = Logger.getLogger(DeleteCouponServlet.class.getName());
	
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
				PropertiesUtil props = new PropertiesUtil();
		String token = req.getParameter("token");
		String action = req.getParameter("action");

		if (token == null || (token != null && !token.equals(props.getProperty("security.token")))) {
			//Display the error message
			log.severe("Invalid token. " + token);
			resp.setContentType("text/plain");
			resp.getWriter().println("{\"status\":\"Invalid Token\"}");
		} else {
			if (action == null || (action != null && !action.equals("coupons"))) {
				deleteCoupons(req, resp);
			}else if (action == null || (action != null && !action.equals("coupondata"))) {
				deleteCouponData(req, resp);
			}
		}
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doPost(req, resp);
	}

	private void deleteCoupons(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		//DELETE COUPONS
		List<Key<Coupon>> couponkeys = ObjectifyService.ofy().load().type(Coupon.class).keys().list();
		ObjectifyService.ofy().delete().keys(couponkeys).now();
		
		log.info("Coupons deleted successfully");
		resp.setContentType("text/plain");
		resp.getWriter().println("{\"status\":\"Coupons deleted successfully\"}");

	}
	
	private void deleteCouponData(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		//DELETE COUPON DATA
		List<Key<CouponData>> couponDataKeys = ObjectifyService.ofy().load().type(CouponData.class).keys().list();
		ObjectifyService.ofy().delete().keys(couponDataKeys).now();
		
		log.info("Coupon Data deleted successfully");
		resp.setContentType("text/plain");
		resp.getWriter().println("{\"status\":\"Coupon Data deleted successfully\"}");

	}
}