package com.possible.google.partnercredits.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.possible.google.partnercredits.email.EmailHelper;

@SuppressWarnings("serial")
public class BlastEmailServlet extends HttpServlet {

	// Process the http POST of the form
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		EmailHelper.sendNotificationMail(9);
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doPost(req, resp);
	}

	
}