<%@ page import="com.google.appengine.api.blobstore.BlobstoreServiceFactory" %>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreService" %>

<%
   	BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
 	String token=request.getParameter("token"); 
%>
<html>
    <head>
        <title>Bulk Import - Partners</title>
    </head>
    <body>
        <form action="<%= blobstoreService.createUploadUrl("/partner-import?token="+token)   %>" method="post" enctype="multipart/form-data">
             <input type="file" name="importPartnerCSV">
            <input type="submit" value="Submit">
        </form>
    </body>
</html>